package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  private List<CellColor> grid;
  int rows;
  int cols;

  public ColorGrid(int rows, int cols) {

    this.rows = rows;
    this.cols = cols;

    grid = new ArrayList<>();
    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        grid.add(new CellColor(new CellPosition(row, col), null));
      }
    }
  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }

  @Override
  public List<CellColor> getCells() {
    return grid;
  }

  @Override
  public Color get(CellPosition pos) {

    if (pos.row() < 0 || pos.col() < 0 || pos.row() >= this.rows || pos.col() >= this.cols) {
      throw new IndexOutOfBoundsException("get CellPosition IndexError");
    }

    for (CellColor value : grid) {
      if (value.cellPosition().equals(pos)) {
        return value.color();
      }
    }
    return null;
  }

  @Override
  public void set(CellPosition pos, Color color) {

    if (pos.row() < 0 || pos.col() < 0 || pos.row() >= this.rows || pos.col() >= this.cols) {
      throw new IndexOutOfBoundsException("set CellPosition IndexError");
    }

    CellColor newCell = new CellColor(pos, color);

    for (int i = 0; i < grid.size(); i++) {
      CellColor value = grid.get(i);
      if (value.cellPosition().equals(pos)) {
        grid.set(i, newCell);

      }
    }
  }

  
  public void addRow(){
    for (int i = 0; i < cols; i ++){
      this.grid.add(new CellColor(new CellPosition(this.rows, i), null));
    }
    this.rows++;
  }
  

    public void addCol(){
      for (int i = 0; i < rows; i ++){
        this.grid.add(new CellColor(new CellPosition(i, this.cols), null));
      }
      this.cols++;
  }


}
