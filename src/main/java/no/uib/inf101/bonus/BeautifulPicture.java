package no.uib.inf101.bonus;

import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class BeautifulPicture extends JPanel {


    private BufferedImage image;
    private Graphics2D g2d;

    public BeautifulPicture(BufferedImage image){
        this.image = image;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        this.g2d = (Graphics2D) g;

        int x = 10;
        int y = 10;
        int x2 = 20;
        int y2 = 20;

        if (image != null) {

            g2d.drawImage(this.image, x, y, this.getWidth() - x2, this.getHeight() - y2, (ImageObserver) this);
        }
    }
}
