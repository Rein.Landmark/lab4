package no.uib.inf101.bonus;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;


public class Main {

  public static void main(String[] args) {
    BufferedImage image = ImageDownload.run(args);
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    BeautifulPicture beautiful = new BeautifulPicture(image);
    frame.add(beautiful);
    frame.setSize(400, 400);
    frame.setVisible(true);
  }
}
