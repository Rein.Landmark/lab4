package no.uib.inf101.bonus;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

public class ImageDownload {

    

    public static BufferedImage run(String[] args) {

        BufferedImage image;
        
        try {
            String URL = "https://git.app.uib.no/uploads/-/system/user/avatar/3435/avatar.png";
            URL url = new URL(URL);

            image = ImageIO.read(url);
            return image;
        } catch (IOException e) {
            System.out.println("Error downloading image.");

        }
        return null;

    }

}
