package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JFrame;

public class GridView extends JPanel {

  ColorGrid iColorGrid;
  Graphics2D graphic;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid iColorGrid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.iColorGrid = (ColorGrid) iColorGrid;

  }

  @Override
  public void paintComponent(Graphics g) {

    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    this.graphic = g2;

    double MARGIN = 30;
    double x = 0;
    double y = 0;
    double width = this.getWidth();
    double height = this.getHeight();

    Shape background = new Rectangle2D.Double(x, y, width, height);
    this.graphic.setColor(MARGINCOLOR);
    this.graphic.fill(background);

    CellPositionToPixelConverter data = new CellPositionToPixelConverter(getBounds(), iColorGrid, MARGIN);
    drawCells(this.graphic, iColorGrid, data);

  }

  private static void drawCells(Graphics2D graphics, CellColorCollection colors, CellPositionToPixelConverter data) {

    List<CellColor> cells = colors.getCells();

    for (CellColor cell : cells) {
      Rectangle2D pixelPosition = data.getBoundsForCell(cell.cellPosition());
      Color color = cell.color();

      double x = pixelPosition.getX();
      double y = pixelPosition.getY();
      double height = pixelPosition.getHeight();
      double width = pixelPosition.getWidth();

      if (color == null) {
        color = Color.DARK_GRAY;
      }

      graphics.setColor(color);
      graphics.fill(new Rectangle2D.Double(x, y, width, height));
    }

  }

}
