package no.uib.inf101.gridview;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition pos) {

    double availableWidth = (this.box.getWidth() - (this.margin * (this.gd.cols() + 1)));
    double availableHeight = (this.box.getHeight() - (this.margin * (this.gd.rows() + 1)));

    double cellWidth = availableWidth / this.gd.cols();
    double cellHeight = availableHeight / this.gd.rows();

    double cellX = box.getX() + this.margin + (this.margin + cellWidth) * pos.col();
    double cellY = box.getY() + this.margin + (this.margin + cellHeight) * pos.row();

    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);

  }

}
